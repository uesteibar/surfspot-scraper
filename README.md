# Surfspot Scraper

Hacky dirty script used to get a collection of geolocated surf spots from [magicseaweed.com](https://magicseaweed.com).

The spots can be found [here](https://github.com/uesteibar/surfspot-scraper/blob/master/spots.json).

## Usage

Clone the repo
```
git clone https://github.com/uesteibar/surfspot-scraper && cd surfspot-scraper
```

Install dependencies
```
mix deps.get
```

Run the script
```
mix spots.get export_file.json
```
