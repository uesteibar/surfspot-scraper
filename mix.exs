defmodule SurfspotScraper.Mixfile do
  use Mix.Project

  def project do
    [
      app: :surfspot_scraper,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [
        :logger,
        :httpoison,
        :hound
      ]
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 0.13.0"},
      {:poison, "~> 3.1"},
      {:floki, "~> 0.19.1"},
      {:hound, "~> 1.0.4"}
    ]
  end
end
