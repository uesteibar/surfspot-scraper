defmodule Mix.Tasks.Spots.Get do
  use Mix.Task

  alias SurfspotScraper.Scraper

  def run([filename | _]) do
    {:ok, _} = Application.ensure_all_started(:httpoison)

    IO.puts("-> Getting all spots from MSW")

    spots = Scraper.get_all()
    File.write(filename, Poison.encode!(spots))

    IO.puts("-> Done! All spots exported to #{filename}")
  end
end
