defmodule SurfspotScraper.Scraper do
  @moduledoc """
  Documentation for SurfspotScraper.
  """

  def get_all do
    1..112
    |> Enum.map(&scrape_area(&1))
    |> Enum.concat()
  end

  defp scrape_area(id) do
    case get_coordinates("https://es.magicseaweed.com/Surf-Forecast/#{id}/") do
      [{_, data, _} | _] ->
        {_, spots} = Enum.find(data, fn element -> match?({"data-collection", _}, element) end)

        spots |> Poison.decode!() |> Enum.map(&normalize_spot(&1))

      _ ->
        []
    end
  end

  defp get_coordinates(url) do
    url
    |> HTTPoison.get!()
    |> Map.get(:body)
    |> Floki.find("div.msw-map")
  end

  def normalize_spot(raw_spot) do
    %{
      msw_id: Map.get(raw_spot, "_id"),
      lat: Map.get(raw_spot, "lat"),
      lon: Map.get(raw_spot, "lon"),
      name: Map.get(raw_spot, "name"),
      description: Map.get(raw_spot, "description"),
      country_code: get_in(raw_spot, ["country", "iso"])
    }
  end
end
